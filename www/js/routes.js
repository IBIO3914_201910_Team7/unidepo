angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('menu', {
    url: '/page4',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('perfil', {
    url: '/perfil',
    templateUrl: 'templates/perfil.html',
    controller: 'perfilCtrl'
  })

  .state('rutina', {
    url: '/page8',
    templateUrl: 'templates/rutina.html',
    controller: 'rutinaCtrl'
  })

  .state('GlTeosGemelos', {
    url: '/gemelos',
    templateUrl: 'templates/GlTeosGemelos.html',
    controller: 'GlTeosGemelosCtrl'
  })

  .state('GlTeosGemelos2', {
    url: '/gluteosiz',
    templateUrl: 'templates/GlTeosGemelos2.html',
    controller: 'GlTeosGemelos2Ctrl'
  })

  .state('GlTeosGemelos3', {
    url: '/gluteosder',
    templateUrl: 'templates/GlTeosGemelos3.html',
    controller: 'GlTeosGemelos3Ctrl'
  })

  .state('GlTeosGemelos4', {
    url: '/gluteosambas',
    templateUrl: 'templates/GlTeosGemelos4.html',
    controller: 'GlTeosGemelos4Ctrl'
  })

  .state('espaldaPecho', {
    url: '/EspaldaPecho1',
    templateUrl: 'templates/espaldaPecho.html',
    controller: 'espaldaPechoCtrl'
  })

  .state('espaldaPecho2', {
    url: '/EspaldaPecho2',
    templateUrl: 'templates/espaldaPecho2.html',
    controller: 'espaldaPecho2Ctrl'
  })

  .state('espaldaPecho3', {
    url: '/espaldaPEcho3',
    templateUrl: 'templates/espaldaPecho3.html',
    controller: 'espaldaPecho3Ctrl'
  })

  .state('configuraciN', {
    url: '/configuracion',
    templateUrl: 'templates/configuraciN.html',
    controller: 'configuraciNCtrl'
  })

  .state('estadisticas', {
    url: '/estadistica',
    templateUrl: 'templates/estadisticas.html',
    controller: 'estadisticasCtrl'
  })

  .state('agregarRutina', {
    url: '/AgregarRutina',
    templateUrl: 'templates/agregarRutina.html',
    controller: 'agregarRutinaCtrl'
  })

  .state('ejercicioNuevo', {
    url: '/ejercicioNuevo',
    templateUrl: 'templates/ejercicioNuevo.html',
    controller: 'ejercicioNuevoCtrl'
  })

  .state('registrate', {
    url: '/registrate ',
    templateUrl: 'templates/registrate.html',
    controller: 'registrateCtrl'
  })

$urlRouterProvider.otherwise('/login')


});